<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-nguyenhoangbach' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ')$T=s{aB?9}FA_x^?{k)!c{3#bH[FYWAh6>[?pMa?ZutU6/489b}EFV&n>;>.<-&' );
define( 'SECURE_AUTH_KEY',  '5u1IKPi<ah/ls%Yv3oRQ-ka>TK(BD<4)*s~VO*{UkGRjCdX6})V(vYo[TeCmSWgx' );
define( 'LOGGED_IN_KEY',    '?%G(&0Ps *6Tl=a,yed-EV`:ud| 9R[%bcE@ x33G*DD$~IcQj~{8`4iQ9$`C?b:' );
define( 'NONCE_KEY',        '&#.K%&0g7>~k6-BdmA?r/G|Vq!KP&Z4({_WLfD g.)Tt9xnLbC&Bl17^;DM@Cmc9' );
define( 'AUTH_SALT',        'XJhLN2eCG&,wA|9}0l,mpt(5uX$TaL#$hL&0Cj~mo=2Z$4 U Pl~< T_(rNBL_wn' );
define( 'SECURE_AUTH_SALT', 'N>*)?xB%mJN9o<F_^l!)[>K2LtS`4ANf:t/&-CCqO_>w9;d6%*YT#MF<0-TJNt0C' );
define( 'LOGGED_IN_SALT',   'O2E>}>N$A:]IJ pFlA#^#SxDoMCzj+YYZSbxG,6{+H;o{}%f=ucfz@u-lW:o5AcX' );
define( 'NONCE_SALT',       '6cmI@Xl:XYPueP/NW=5j6jk}3)#[J|P?/bj!<;oI@,hrV?AM<rc8Z%0YTt?bqLG&' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
